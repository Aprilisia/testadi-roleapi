﻿using RoleApi.Models;

namespace RoleApi.Interfaces
{
    public interface IRole
    {
        List<RoleModel> GetRole();
    }
}
