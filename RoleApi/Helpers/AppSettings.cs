﻿namespace RoleApi.Helpers
{
    public class AppSettings
    {
        public string ConnectionString { get; set; }
        public string Environment { get; set; }
        public string BaseUrl { get; set; }
    }
}
