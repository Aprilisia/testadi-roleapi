﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using RoleApi.Interfaces;
using RoleApi.Models;


namespace RoleApi.Controllers
{
    [Route("api/role")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private IRole _role;
        public RoleController(IRole role) {
            _role = role;
        }

        [HttpGet]
        public IActionResult GetRole()
        {
            var data = _role.GetRole();
            return new JsonResult(new { status = 200, data = data });
        }
    }
}
