﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace RoleApi.Models
{
    [Table("tbl_role")]
    public class RoleModel
    {
        public int role_id { get; set; }
        public string name { get; set; }
    }
}
