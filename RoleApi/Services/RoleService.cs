﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Options;
using RoleApi.Helpers;
using RoleApi.Interfaces;
using RoleApi.Models;

namespace RoleApi.Services
{
    public class RoleService : IRole
    {
        private readonly Helpers.AppSettings _appSettings;
        public RoleService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public List<RoleModel> GetRole()
        {
            List<RoleModel> result = new List<RoleModel>();
            var sql = "";
            try
            {
                using (var cs = new ConnectionService(_appSettings).GetConnection())
                {
                    sql = "SELECT " +
                            "A.role_id, A.name " +
                            "FROM tbl_role A";

                    SqlCommand command = new SqlCommand(sql, cs);
                    cs.Open();
                    var reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            result.Add(new RoleModel()
                            {
                                role_id = Convert.ToInt32(reader["role_id"].ToString()),
                                name = reader["name"] == DBNull.Value ? null : reader["name"].ToString(),
                            });
                        }
                    }
                    cs.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return result;
        }
    }
}
